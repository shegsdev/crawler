import json
import scrapy
from .thread import worker
from .helpers import read_content, append_to_file, file_to_set
from .validation import validate_url
from urllib.parse import urlparse


class Crawler(scrapy.Spider):
    name = "punch"
    start_urls = [
        'http://www.businessday.ng/',
        'http://www.vanguardngr.com/',
        'http://dailypost.ng/',
        'https://punchng.com/',
        'http://saharareporters.com/',
        'https://www.premiumtimesng.com/',
        'https://www.latestnigeriannews.com/',
        'https://guardian.ng/',
        'https://www.dailytrust.com.ng/',
        'https://www.thisdaylive.com/',
        'https://sunnewsonline.com/',
        'https://www.nigeriasun.com/',
        'https://www.tribuneonlineng.com/',
        'https://1push.ng/nigeria-newspaper/tribune-newspaper/',
        'https://www.naijadailies.com/News/Paper/Nigerian-Tribune-11',
        'https://leadership.ng/',
        'http://thenationonlineng.net/',
        'https://dailytimes.ng/',
        'http://www.aitonline.tv/news',
        'https://www.channelstv.com/',
    ]

    search_keywords = ["fccpc", "cpc", "Consumer Protection Council"]

    json_file = list()
    visited_urls = set()

    def parse(self, response):
        homepage_urls = response.css('a::attr(href)').getall()

        parsed_uri = urlparse(response.url)
        domain_name = '{uri.netloc}'.format(uri=parsed_uri)
        print("domain name", domain_name)

        # Loading all visited urls on first load
        if len(self.visited_urls) < 1:
            self.visited_urls = file_to_set('visited.txt')
        print("******* visited urls count: : ***** ", len(self.visited_urls))

        for href in homepage_urls:
            if href not in self.visited_urls:
                self.visited_urls.add(href)
                if validate_url(domain_name, href):
                    append_to_file('visited.txt', href)
                    data = read_content(href, self.search_keywords)
                    if data is not None:
                        worker(Crawler.save_json('crawled.json', data))
                    yield response.follow(href, callback=self.parse)

    @staticmethod
    def save_json(filename, data_list):
        # read the file and load into list
        with open(filename, 'r') as infile:
            try:
                results = json.loads(infile.read(), encoding="utf-8")
                results += data_list

                with open(filename, 'w') as outfile:
                    json.dump(results, outfile, indent=2)
            except:
                with open(filename, 'w') as empty:
                    empty.write('[]')


