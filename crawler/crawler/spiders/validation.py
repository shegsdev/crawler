import validators


def validate_url(domain, url):
    if (url is not None) and url.find(domain) and validators.url(url) and validators.domain:
        return url
    pass

