import os
import re
import time
from datetime import datetime
from newspaper import Article


def create_project_dir(directory):
    if not os.path.exists(directory):
        print('Creating project ' + directory)
        os.makedirs(directory)


def create_data_files(project_name, base_url):
    queue = project_name + '/queue.txt'
    crawled = project_name + '/crawled.txt'
    if not os.path.isfile(queue):
        write_file(queue, base_url)
    if not os.path.isfile(crawled):
        write_file(crawled, '')


def write_file(path, data):
    f = open(path, 'w')
    f.write(data)
    f.close()


# create_data_files('newz_data', 'https://edition.cnn.com')

# Add data onto an existing file
def append_to_file(path, data):
    with open(path, 'a') as file:
        file.write(data + '\n')


# Delete the contents of a file
def delete_file_contents(path):
    with open(path, 'w'):
        pass


# Read a file and convert each line to set items
def file_to_set(file_name):
    results = set()
    with open(file_name, 'rt') as f:
        for line in f:
            results.add(line.replace('\n', ''))
    return results


# Iterate through a set, each item will be a new line in the file
def set_to_file(links, file):
    delete_file_contents(file)
    for link in sorted(links):
        append_to_file(file, link)


def read_content(url, keywords):
    try:
        content = Article(url, keep_article_html=True, memoize_articles=False)
        content.download()
        content.parse()
        time.sleep(1)
        # content.nlp()

        json = list()

        for keyword in keywords:
            regex = r'\b' + re.escape(keyword) + r'\b'
            match = re.search(regex, content.article_html, re.IGNORECASE)

            if match:
                date = datetime.now()
                print("\n\n\t\t\t==========********** Keyword found **********===========\n\n")
                json.append({
                    "title": content.title,
                    "link": url,
                    "created_parsed": date.strftime("%B %d %Y"),
                    "keywords": keyword,
                    "author": content.authors,
                    "text": content.summary,
                    "full_text": content.text,
                    "article_image": content.top_image,
                    "source_link": content.source_url
                })
            else:
                continue
        return json
    except:
        return None
